<h1>Melbourne Integrative Genomics RNASeq GATK best practices pipeline.</h1>

<h2>Getting started</h2>
To start, create a directory on helix in your working directory.
Clone this repository into that directory

If you are using the pipeline for Human samples you can download the GATK resource bundle.

Create a directory called "reference" in your working directory and download the bundle files directly there.

https://software.broadinstitute.org/gatk/download/bundle

Follow the instructions on that page for downloading.

If you are using a non-standard reference you must download the relevant files.

The main files you will need for the reference are:
<ul>
<li>Homo_sapiens.GRCh38.90.gtf</li>
<li>Homo_sapiens.GRCh38.p10.ensemblv90.dna.primary_assembly.dict</li>
<li>Homo_sapiens.GRCh38.p10.ensemblv90.dna.primary_assembly.fa</li>
<li>Homo_sapiens.GRCh38.p10.ensemblv90.dna.primary_assembly.fa.fai</li>
</li>
</ul>

Some basic instructions on how to build a reference for GATK analysis can be found
here: https://software.broadinstitute.org/gatk/documentation/article.php?id=8017

And for older versions can be found 
here: https://gatkforums.broadinstitute.org/gatk/discussion/2798/howto-prepare-a-reference-for-use-with-bwa-and-gatk

<h2>How to run the pipeline</h2>

1) Look at the inputBamTSV.txt file that comes with this repository.
You will need to format this file to include your bam files.
The first column will be the sample name and the second directory is the name of the bamfile. eg.
The third column, which is the index for the bam file, is optional.

```
MPI-025 uniquelyMapped_STAR_Hg38_second_paired_trimmedOutput_MPI-025Aligned.sortedByCoord.out.bam       uniquelyMapped_STAR_Hg38_second_paired_trimmedOutput_MPI-025Aligned.sortedByCoord.out.bam.bai
MPI-048 uniquelyMapped_STAR_Hg38_second_paired_trimmedOutput_MPI-048Aligned.sortedByCoord.out.bam       uniquelyMapped_STAR_Hg38_second_paired_trimmedOutput_MPI-048Aligned.sortedByCoord.out.bam.bai
```

2) The next step is to open up the rnaseqBamToGCVF.json and edit the parameters to suit your analysis.
The first step would be to change the "./" in the first six parameters to include the full path to the required input files

``` 
"##_COMMENT_1": "Human Bam to VCF merged vcf for human rnaseq",
  "rnaseqBamToGCVF.refIndex": "./reference/Homo_sapiens.GRCh38.p10.ensemblv90.dna.primary_assembly.fa.fai",
  "rnaseqBamToGCVF.refDict": "./reference/Homo_sapiens.GRCh38.p10.ensemblv90.dna.primary_assembly.dict",
  "rnaseqBamToGCVF.gatkLocation": "./GenomeAnalysisTK.jar",
  "rnaseqBamToGCVF.refFasta": "./Homo_sapiens.GRCh38.p10.ensemblv90.dna.primary_assembly.fa",
  "rnaseqBamToGCVF.inputSamplesFile": "./inputBamTSV.txt",
  "rnaseqBamToGCVF.picardLocation": "./picard.jar",
  ```

You can find the path by using "echo $PWD" in the working directory. e.g.
[bshaban@snowy-sg1 rnaseq]$ echo $PWD
/vlsci/SG0009/bshaban/gitlab/gatk/rnaseq

3) The next step is to optimise the runMinutes, runThreads and runMem for each of the stages of the pipeline. I have optimised as much as possible for the pipeline to work efficiently for two bam files or 74 of average size of 2-4gb. Files any bigger than this and the pipeline may need to be adjusted accordingly

The lines in the json file that refer to the minutes/threads/mem are as follows:
These parameters are the ones which are passed to slurm for job submission.
Minutes referes to the job run time, threads is the requested threads, and mem is the requested memory in Mb.

```
"rnaseqBamToGCVF.picard.picardRunMinutes": 400,
  "rnaseqBamToGCVF.picard.picardThreads": 6,
  "rnaseqBamToGCVF.picard.picardMem": 40000,

"rnaseqBamToGCVF.picardMarkDuplicates.outputMetrics": "output.metrics",
  "rnaseqBamToGCVF.picardMarkDuplicates.picardMarkDuplicatesThreads": 6,
  "rnaseqBamToGCVF.picardMarkDuplicates.picardMarkDuplicatesMem": 40000,

"rnaseqBamToGCVF.splitNCigarReads.splitNCigarReadsThreads": 6,
  "rnaseqBamToGCVF.splitNCigarReads.splitNCigarReadsRunMinutes": 400,
  "rnaseqBamToGCVF.splitNCigarReads.splitNCigarReadsMem": 40000,

 "rnaseqBamToGCVF.HaplotypeCallerERC.haplotypeCallerThreads": 6,
  "rnaseqBamToGCVF.HaplotypeCallerERC.haplotypeCallerRunMinutes": 400,
  "rnaseqBamToGCVF.HaplotypeCallerERC.haplotypeCallerMem": 40000,

 "rnaseqBamToGCVF.GenotypeGVCFs.genotypeRunMinutes": 36000,
  "rnaseqBamToGCVF.GenotypeGVCFs.genotypeThreads": 6,
  "rnaseqBamToGCVF.GenotypeGVCFs.genotypeMem": 120000,

 "rnaseqBamToGCVF.createPicardBamIndex.createPicardBamIndexRunMinutes": 400,
  "rnaseqBamToGCVF.createPicardBamIndex.createPicardBamIndexThreads": 6,
  "rnaseqBamToGCVF.createPicardBamIndex.createPicardBamIndexMem": 40000,
  "rnaseqBamToGCVF.createRefIndex.createRefIndexRunMinutes": 400,
  "rnaseqBamToGCVF.createRefIndex.createRefIndexRunThreads": 6,
  "rnaseqBamToGCVF.createRefIndex.createRefIndexMem": 40000,
  "rnaseqBamToGCVF.createBamIndex.createBamIndexRunMinutes": 400,
  "rnaseqBamToGCVF.createBamIndex.createBamIndexThreads": 6,
  "rnaseqBamToGCVF.createBamIndex.createBamIndexMem": 40000,
  "rnaseqBamToGCVF.combineGVCFs.combineRunMinutes": 36000,
  "rnaseqBamToGCVF.combineGVCFs.combineRunThreads": 6,
  "rnaseqBamToGCVF.combineGVCFs.combineRunMem": 120000
  ```

4) To run the pipeline, you can run it directly from SG1 *NOTE* You can only run on SG1 if you are running with the config file (cromslurm.conf). 
If you are not using cromslurm.cong you MUST use sinteractive to request resources. cromslurm.conf allows the pipeline to be run with helix's job scheduler, i.e. slurm.

5) To run the pipeline you can use the following line in the working directory.

java -Dconfig.file=./cromslurm.conf -jar ./cromwell-31.jar run rnaseqBamToGCVF.wdl -i rnaseqBamToGCVF.json

A successful run will result in the following output print to the screen at the end of the stdout.


 ``` 
 "outputs": {
    "rnaseqBamToGCVF.createPicardBamIndex.mDBamIndex": ["/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-picardMarkDuplicates/shard-0/execution/MPI-025.dedupped.bai", "/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-picardMarkDuplicates/shard-1/execution/MPI-048.dedupped.bai"],
    "rnaseqBamToGCVF.picardMarkDuplicates.picardDeduppedBam": ["/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-picardMarkDuplicates/shard-0/execution/MPI-025.dedupped.bam", "/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-picardMarkDuplicates/shard-1/execution/MPI-048.dedupped.bam"],
    "rnaseqBamToGCVF.splitNCigarReads.splitCigarsBamOutput": ["/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-splitNCigarReads/shard-0/execution/MPI-025.splitCigars.bam", "/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-splitNCigarReads/shard-1/execution/MPI-048.splitCigars.bam"],
    "rnaseqBamToGCVF.combineGVCFs.combinedOutput": "/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-combineGVCFs/execution/combinedGVCFs.cohort.g.vcf",
    "rnaseqBamToGCVF.createBamIndex.splitCigarsBamOutput": ["/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-splitNCigarReads/shard-0/execution/MPI-025.splitCigars.bai", "/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-splitNCigarReads/shard-1/execution/MPI-048.splitCigars.bai"],
    "rnaseqBamToGCVF.GenotypeGVCFs.rawVCF": "/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-GenotypeGVCFs/execution/CEUtrio_rawVariants.vcf",
    "rnaseqBamToGCVF.createRefIndex.refFastaIndex": ["/vlsci/SG0009/bshaban/gatk/irene/reference/Homo_sapiens.GRCh38.p10.ensemblv90.dna.primary_assembly.fa.fai", "/vlsci/SG0009/bshaban/gatk/irene/reference/Homo_sapiens.GRCh38.p10.ensemblv90.dna.primary_assembly.fa.fai"],
    "rnaseqBamToGCVF.HaplotypeCallerERC.GVCF": ["/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-HaplotypeCallerERC/shard-0/execution/MPI-025_rawLikelihoods.g.vcf", "/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-HaplotypeCallerERC/shard-1/execution/MPI-048_rawLikelihoods.g.vcf"],
    "rnaseqBamToGCVF.picard.picardOutputBam": ["/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-picard/shard-0/execution/MPI-025Aligned.bam", "/vlsci/SG0009/bshaban/gatk/irene/finishing/cromwell-executions/rnaseqBamToGCVF/9419a2f4-a584-4ffb-b593-b7bde08dcf7c/call-picard/shard-1/execution/MPI-048Aligned.bam"]
  },
  "id": "9419a2f4-a584-4ffb-b593-b7bde08dcf7c"
  ```
  
  The files you will be interested in will be in the locations above or will be copied to your working directory.
  
  The workflow was derived from the GATK best practices found here: https://software.broadinstitute.org/gatk/documentation/article.php?id=3891 and https://gatkforums.broadinstitute.org/gatk/discussion/4067/best-practices-for-variant-discovery-in-rnaseq
  
  If you have anyquestions please email Bobbie Shaban bshaban@unimelb.edu.au
  
  