## rnaseq GATK best practices pipeline
## Bobbie Shaban
## 10/01/2018
## Melbourne Integrative genomics
##
##
##
##
##
##
##
##
##
##
##
workflow rnaSeq {
    	File inputFastqRead1
    	File inputFastqRead2
    	String threads
        String sampleName
	String genomeDir
	String picardLocation
	String gatkLocation
	File refFasta
 	String refFastaSampleName

	call fastqc { 
	    input: 
		threads=threads,
		inputFastqRead1=inputFastqRead1,
		inputFastqRead2=inputFastqRead2,
		sampleName=sampleName
	}

	call star {
	    input:
		threads=threads, 
		genomeDir=genomeDir,
		inputFastqRead1=inputFastqRead1,
                inputFastqRead2=inputFastqRead2,
		sampleName=sampleName	
	}

	call picard {
	    String typeARRG
	    String sortOrder
	    String readGroupID
	    String readGroupLibrary
	    String readGroupPlatform
	    String readGroupPlatformBarcode

	    input: 
		picardLocation=picardLocation,
		picardInputSam=star.starOutputSam,
		sampleName=sampleName
	}

	call picardMarkDuplicates {
	   String typeMD
	   String createIndex
	   String validationStringency
	   String outputMetrics

            input:
		picardLocation=picardLocation,
                picardMDInputBam=picard.picardOutputBam,
                sampleName=sampleName	
	}
	
	call createPicardBamIndex {
   	    input:
		picardMDBamToBeIndexed=picardMarkDuplicates.picardDeduppedBam
	}

	call createRefIndex{
	   input:
		refFasta=refFasta
	}
	
	call createSequenceDictionary {
            input:
                refFasta=refFasta,
		refFastaSampleName=refFastaSampleName,
                picardLocation=picardLocation,
		genomeDir=genomeDir
        }


	call splitNCigarReads{
	    String splitCigars
	    String RF
	    String RMQF
	    String RMQT
	    String U	    	
		
	    input:
		refFasta=refFasta,
                refFastaIndex=createRefIndex.refFastaIndex,
		refDictionary=createSequenceDictionary.refSequenceDict,
                sampleName=sampleName,
                gatkLocation=gatkLocation,
		splitCigarsInputBam=picardMarkDuplicates.picardDeduppedBam,
		splitCigarsInputBamIndex=createPicardBamIndex.mDBamIndex
	}
	
	call createBamIndex{
	    input:
		bamToBeIndexed=splitNCigarReads.splitCigarsBamOutput	
	}

	call variantCalling {
	  String variantCallType
	  String standCallConf 

	    input:
		refFasta=refFasta,
		refFastaIndex=createRefIndex.refFastaIndex,
		refDictionary=createSequenceDictionary.refSequenceDict,
		sampleName=sampleName,
		gatkLocation=gatkLocation,
		variantInputBam=splitNCigarReads.splitCigarsBamOutput,	
		variantInputBamIndex=createBamIndex.splitCigarsBamOutput
	}
}

task fastqc {
	String threads
	File inputFastqRead1
	File inputFastqRead2
	String sampleName

	command {
		fastqc \
			-t ${threads} \
			${inputFastqRead1} \
			${inputFastqRead2} 
	}
}

task star {
	String threads
	String genomeDir
	String sampleName
	File inputFastqRead1
	File inputFastqRead2		

	command {
		STAR --genomeDir ${genomeDir} \
		     --runThreadN ${threads} \
		     --readFilesIn ${inputFastqRead1} ${inputFastqRead2} \
		     --outFileNamePrefix ${sampleName}
	}
	output {
		File starOutputSam="${sampleName}Aligned.out.sam"
	}
}

task picard {
        String typeARRG
        String sampleName
        String sortOrder
        String readGroupID
        String readGroupLibrary
        String readGroupPlatform
        String readGroupPlatformBarcode
        String picardLocation
	File picardInputSam

        command {
                java -jar ${picardLocation} ${typeARRG} \
                        I=${picardInputSam} \
                        O=${sampleName}Aligned.bam \
                        SO=${sortOrder} \
                        RGID=${readGroupID} \
                        RGLB=${readGroupLibrary} \
                        RGPL=${readGroupPlatform} \
                        RGPU=${readGroupPlatformBarcode} \
                        RGSM=${sampleName}
        }
        output {
                File picardOutputBam="${sampleName}Aligned.bam"
        }
}
 
task picardMarkDuplicates {
	String typeMD
	String sampleName
	String picardLocation
	String createIndex
	String validationStringency
	String outputMetrics
	File picardMDInputBam

	command {
		java -jar ${picardLocation} ${typeMD} \
		     I=${picardMDInputBam} \
		     O="${sampleName}.dedupped.bam" \
		     CREATE_INDEX=${createIndex} \
		     VALIDATION_STRINGENCY=${validationStringency} \
		     M=${outputMetrics}
	}
	output {
		File picardDeduppedBam="${sampleName}.dedupped.bam"
	}
}

task createPicardBamIndex {
	File picardMDBamToBeIndexed		

	command {
		samtools index ${picardMDBamToBeIndexed}
	}
	output {
		File mDBamIndex=sub("${picardMDBamToBeIndexed}", "bam", "bai")
	}
}

task createRefIndex {
	File refFasta
	
	command {
		samtools faidx ${refFasta}
	}
	output{
		File refFastaIndex = "${refFasta}.fai"
	}
}

task createSequenceDictionary {
        File refFasta
	String refFastaSampleName
        String picardLocation
	String genomeDir

        command {
                java -jar ${picardLocation} CreateSequenceDictionary \
                        REFERENCE=${refFasta} \
                        OUTPUT="${genomeDir}/${refFastaSampleName}.dict"
        }
        output {
                File refSequenceDict="${genomeDir}/${refFastaSampleName}.dict"
        }
}

task splitNCigarReads{
        String splitCigars
        String sampleName
        String gatkLocation
        String RF
        String RMQF
        String RMQT
        String U
        File refFasta
	File refFastaIndex
	File refDictionary
        File splitCigarsInputBam
	File splitCigarsInputBamIndex

        command {
                java -jar ${gatkLocation} \
                        -T ${splitCigars} \
                        -R ${refFasta} \
                        -I ${splitCigarsInputBam} \
                        -o ${sampleName}.splitCigars.bam \
                        -rf ${RF} \
                        -RMQF ${RMQF} \
                        -RMQT ${RMQT} \
                        -U ${U}
        }
        output {
                File splitCigarsBamOutput="${sampleName}.splitCigars.bam"
        }
}

task createBamIndex {
	File bamToBeIndexed

	command {
		samtools index ${bamToBeIndexed}
	}
	output {
		File splitCigarsBamOutput=sub("${bamToBeIndexed}", "bam", "bai")
	}	
}

task variantCalling {
        String sampleName
        String variantCallType
	String gatkLocation
	String standCallConf
        File refFasta
	File refFastaIndex
	File refDictionary
        File variantInputBam
	File variantInputBamIndex

        command {
                java -jar ${gatkLocation} \
                        -T ${variantCallType} \
                        -R ${refFasta} \
                        -I ${variantInputBam} \
                        -dontUseSoftClippedBases \
                        -stand_call_conf ${standCallConf} \
                        -o ${sampleName}.raw.vcf
        }
        output {
                File rawVCF = "${sampleName}.raw.vcf"
        }
}
