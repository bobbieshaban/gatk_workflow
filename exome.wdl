## Exome GATK best practices pipeline
## Bobbie Shaban
## 18/01/2018
## Melbourne Integrative genomics
##
##
#

workflow exome{
	File inputFastqRead1
        File inputFastqRead2
	String gatkLocation
	String threads
	
	call fastqc {
		input: 
		threads=threads,
		inputFastqRead1=inputFastqRead1,
		inputFastqRead2=inputFastqRead2
	}	
}

	task fastqc {
		String threads
        	File inputFastqRead1
        	File inputFastqRead2

       		command {
                	fastqc \
                        	-t ${threads} \
                        	${inputFastqRead1} \
                        	${inputFastqRead2}
        	}
	}


